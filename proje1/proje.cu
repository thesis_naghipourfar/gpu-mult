#include "cuda_runtime.h"
#include "device_launch_parameters.h"
# include <limits.h>
# include <string.h>
#include <stdio.h>
#include<iostream>
#include<fstream>
#include <sstream>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>
#include <chrono>
#include <vector>
#include <math.h>
#include <time.h>


using namespace std;
#define MAX_THREADS_PER_BLOCK 512

int no_of_nodes = 10;
int edge_list_size = 10;
int  f = 0, r = -1;

#define N 200
int *a;
int *b;
int *product;


int main()
{

	

	a = (int *)malloc(N * N * sizeof(int));
	b = (int *)malloc(N * N * sizeof(int));
	product = (int *)malloc(N * N * sizeof(int));

	bool stop;
	stop = false;
	



	///////////////////////////////////////////////////////////////
	auto start = chrono::steady_clock::now();

	srand(time(NULL));

	int  i, j, k;

	cout << "The first matrix is:" << endl;
	for (i = 0; i < N; ++i) {
		for (j = 0; j < N; ++j)
		{
			*(a + i*N + j) = round(rand() / 1000);
			cout << *(a + i*N + j) << " ";
		}

		cout << endl;
	}

	cout << "The second matrix is:" << endl;
	for (i = 0; i < N; ++i) {
		for (j = 0; j < N; ++j)
		{
			*(b + i*N + j) = round(rand() / 1000);
			cout << *(b + i*N + j) << " ";
		}

		cout << endl;
	}

	for (i = 0; i<N; ++i)
	for (j = 0; j < N; ++j) {
		*(product + i*N + j) = 0;
	}
	for (i = 0; i<N; ++i)
	for (j = 0; j<N; ++j)
	for (k = 0; k<N; ++k) {
		*(product + i*N + j) += *(a + i*N + k)* *(b + k*N + j);
	}
	cout << "Product of the two matrices is:" << endl;
	for (i = 0; i<N; ++i) {
		for (j = 0; j<N; ++j)
			cout << *(product + i*N + j) << " ";
		cout << endl;
	}

	auto end = chrono::steady_clock::now();
	cout << "\nElapsed time in milliseconds : "
		<< chrono::duration_cast<chrono::milliseconds>(end - start).count()
		<< " ms" << endl;
	getchar();

	free(a);
	free(b);
	free(product);

	return 0;
}
